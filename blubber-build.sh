#!/usr/bin/env bash

# clean up previous builds
docker rm wikispeech-sox-proxy
docker rmi --force wikispeech-sox-proxy

docker rm wikispeech-sox-proxy-test
docker rmi --force wikispeech-sox-proxy-test

# build docker
docker build --tag wikispeech-sox-proxy-test --file .pipeline/blubber.yaml --target test .
docker build --tag wikispeech-sox-proxy --file .pipeline/blubber.yaml --target production .
